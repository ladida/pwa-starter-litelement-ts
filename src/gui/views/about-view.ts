import { html, customElement } from 'lit-element';
import { WithoutShadowDomBaseComp } from '../../core/gui/WithoutShadowDomBaseComp';

@customElement('about-view')
class AboutView extends WithoutShadowDomBaseComp {

  render() {
    return html`
      <div style="padding: 12px">
        <p>This is a PWA starter.</p>
        <p>Developed with Lit-Element and TypeScript.</p>
      </div>
    `;
  }
}