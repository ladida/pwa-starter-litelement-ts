import { html, customElement, property } from 'lit-element';
import { ServiceWorkerInstallationStateServiceFactory, ServiceWorkerInstallationStateService, ServiceWorkerInstallationStateType } from '../../core/service/state/ServiceWorkerInstallationStateService';
import { IStateObserver } from '../../core/service/state/IStateObserver';
import '../components/button/common/white-button-comp';
import { BaseComp } from '../../core/gui/BaseComp';

@customElement('serviceworker-installation-view')
class ServiceWorkerInstallationView extends BaseComp implements IStateObserver {

  @property()
  private state: string;

  private serviceWorkerInstallationStateService: ServiceWorkerInstallationStateService;

  constructor() {
    super();
  }

  protected async initialize() {
    super.initialize();
    this.serviceWorkerInstallationStateService = await ServiceWorkerInstallationStateServiceFactory.getInstance();
    this.serviceWorkerInstallationStateService.registerObserver(this);
  }

  async stateChanged() {
    this.state = this.serviceWorkerInstallationStateService.getState();
    console.log("new state: " + this.state);
  }

  render() {
    return html`
      <div style="position: fixed; width: 100%; z-index: 1; bottom: 0; color: #fff !important; background-color: #616161 !important">
        ${this.state == ServiceWorkerInstallationStateType.INSTALLED ? html`${this.installedTemplate}` : html``}
        ${this.state == ServiceWorkerInstallationStateType.ACTIVATING ? html`${this.activatingTemplate}` : html``}
      </div>
    `;
  }

  get installedTemplate() {
    return html`
      <style>
        .btns {
          display: flex;
        }
        .btn {
          width: 70px;
        }
        @media only screen and (max-width: 500px) {
          .btns {
            flex-direction: column;
          }
        }
      </style>
      <div style="min-width: 300px; max-width: 800px; margin: auto;">
        <div style="display: flex; float: right; margin: 16px">
          <div>
            A new version is available. Activate and restart to get the latest features.
          </div>
          <div class="btns">
            <white-button-comp title="ok" @click="${this.restart}" class="btn" style="margin-left: 8px; margin-bottom: 8px;"></white-button-comp>
            <white-button-comp title="later" @click="${this.later}" class="btn" style="margin-left: 8px; margin-bottom: 8px;"></white-button-comp>
          </div>
        </div>
      </div>
    `;
  }

  get activatingTemplate() {
    return html`
      <div style="width: 100vw; height: 100vh; z-index: 100;">
        <div style="width: max-content; margin: auto; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
          activating and reload
        </div>
      </div>
    `;
  }

  private restart() {
    this.serviceWorkerInstallationStateService.activate();
  }

  private later() {
    this.state = "";
  }
}