import { html, customElement } from 'lit-element';
import { WithoutShadowDomBaseComp } from '../../core/gui/WithoutShadowDomBaseComp';

@customElement('demo-view')
class DemoView extends WithoutShadowDomBaseComp {

  render() {
    return html`
      <div style="padding: 12px">
        <p>Demo view.</p>
      </div>
    `;
  }
}