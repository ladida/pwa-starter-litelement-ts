import { html, css, customElement } from 'lit-element';
import { NavigationStateService, NavigationStateServiceFactory } from '../../core/service/state/NavigationStateService';
import { BaseComp } from '../../core/gui/BaseComp';
import './../components/sidenav/sidenav-comp';
import '../components/button/menu/menu-button-comp';
import { SidenavComp } from './../components/sidenav/sidenav-comp';


@customElement('header-view')
class HeaderView extends BaseComp {

  private navigationStateService: NavigationStateService;

  constructor() {
    super();
  }

  protected async initialize() {
    super.initialize();
    this.navigationStateService = await NavigationStateServiceFactory.getInstance();
  }

  render() {
    return html`
      <div class="container">
        <sidenav-comp id="sidenav">
          <menu-button-comp @click="${this.openDemoView}" title="Demo"></menu-button-comp>
          <menu-button-comp @click="${this.openAboutView}" title="About"></menu-button-comp>
        </sidenav-comp>

        <div class="top-bar">
          <div class="top-bar-content">
            <div class="side-nav-btn" title="Seitenmenü öffnen" @click="${this.openSideNav}">&#9776;</div>
            <span class="title">PWA starter</span>
          </div>
        </div>

        <div class="nav-large" >
          <menu-button-comp @click="${this.openDemoView}" title="Demo"></menu-button-comp>
          <menu-button-comp @click="${this.openAboutView}" title="About"></menu-button-comp>
        </div>
      </div>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }

      .container {
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
        color: #fff !important;
        background-color: #616161 !important;
      }
      .top-bar {
        border-bottom: 1px solid #ccc!important;
      }
      .top-bar-content {
        min-width: 300px;
        max-width: 800px;
        margin: auto;
        box-sizing: border-box;
        display: flex;
      }
      .side-nav-btn {
        padding: 12px 16px;
        cursor: pointer;
      }
      .side-nav-btn:hover {
        background-color: #777777;
      }
      .title {
        padding: 12px 16px;
        font-weight: 600;
      }
      .nav-large {
        display: none;
        min-width: 300px;
        max-width: 800px;
        margin: auto;
      }
      @media only screen and (min-width: 600px) {
        .side-nav-btn {
          display: none;
        }
        .nav-large {
          display: flex;
        }
      }
    `;
  }

  private openSideNav() {
    var sidenav = this.getElementById('sidenav') as SidenavComp;
    if (sidenav) {
      sidenav.openSideNav();
    }
  }

  private closeSideNav() {
    var sidenav = this.getElementById('sidenav') as SidenavComp;
    if (sidenav) {
      sidenav.closeSideNav();
    }
  }

  private openDemoView() {
    this.navigationStateService.setDemoView();
    this.closeSideNav();
  }

  private openAboutView() {
    this.navigationStateService.setAboutView();
    this.closeSideNav();
  }
}