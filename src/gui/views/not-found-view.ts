import { html, customElement } from 'lit-element';
import { WithoutShadowDomBaseComp } from '../../core/gui/WithoutShadowDomBaseComp';

@customElement('not-found-view')
class NotFoundView extends WithoutShadowDomBaseComp {

  render() {
    return html`
      <h1>Not found!</h1>
    `;
  }
}