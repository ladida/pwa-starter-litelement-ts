import { html, css, customElement, property } from 'lit-element';
import { BaseComp } from '../../../../core/gui/BaseComp';

@customElement('menu-button-comp')
export class MenuButtonComp extends BaseComp {

  @property()
  public title: string;

  render() {
    return html`
      <div class="btn">${this.title}</div>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .btn {
        text-align: center;
        padding: 8px 16px;
        cursor: pointer;
      }
      .btn:hover {
        background-color: #777777;
      }
    `;
  }
}