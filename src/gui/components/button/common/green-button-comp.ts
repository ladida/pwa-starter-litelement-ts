import { customElement } from 'lit-element';
import { ButtonComp } from './button-comp';

@customElement('green-button-comp')
export class GreenButtonComp extends ButtonComp {

  constructor() {
    super();

    this.bgColor = '#4CAF50';
    this.textColor = 'white';
  }
}