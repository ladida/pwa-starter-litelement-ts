import { html, css, customElement, property } from 'lit-element';
import { BaseComp } from '../../../../core/gui/BaseComp';

@customElement('button-comp')
export class ButtonComp extends BaseComp {
  @property()
  public title: string;

  @property()
  protected bgColor = 'black';

  @property()
  protected textColor = 'white';

  render() {
    return html`
      <div class="container" style="background-color: ${this.bgColor};">
        <div class="title" style="color: ${this.textColor};">
          ${this.title}
        </div>
      </div>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        cursor: pointer;
        border-radius: 12px;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
      }
      .container:hover {
        opacity: 0.7;
      }
      .title {
        width: max-content;
        padding: 8px;
        margin: auto;
        user-select: none;
      }
    `;
  }
}