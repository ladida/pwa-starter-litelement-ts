import { customElement } from 'lit-element';
import { ButtonComp } from './button-comp';

@customElement('red-button-comp')
export class RedButtonComp extends ButtonComp {

  constructor() {
    super();

    this.bgColor = '#f44336';
    this.textColor = 'white';
  }
}