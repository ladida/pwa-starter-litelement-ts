import { html, css, customElement } from 'lit-element';
import { BaseComp } from '../../../core/gui/BaseComp';

@customElement('sidenav-comp')
export class SidenavComp extends BaseComp {

  render() {
    return html`
      <nav id="side-nav" class="sidenav fullscreen">
        <div class="sidenav-close-btn" @click="${this.closeSideNav}">&times;</div>
        <div class="sidenav-btn-list">
          <slot></slot>
        </div>
      </nav>
      
      <!-- Overlay für Seitenmenü -->
      <div id="nav-overlay" class="overlay fullscreen" @click="${this.closeSideNav}" style="cursor: pointer;"></div>
    `;
  }

  static get styles() {
    return css`
      .closed {
        width: 0px !important;
        height: 0px !important;
      }
      
      .fullscreen {        
        display: none;
        position: fixed;
        width: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
      }
      .sidenav {        
        width: 200px !important;
        background-color: #fff;
        z-index: 9;
        overflow: auto;
        color: #fff !important;
        background-color: #616161 !important;
        
        animation: animateleft 0.4s;
      }
      @keyframes animateleft {
        from {
          left: -300px;
          opacity: 0
        }
        to {
          left: 0;
          opacity: 1
        }
      }
      .sidenav-close-btn {
        display: inline-block;
        vertical-align: middle;
        text-align: center;
        cursor: pointer;
        padding: 8px 16px;
        font-size: 20px;
      }
      .sidenav-close-btn:hover {
        background-color: #7c7777;
      }
      .sidenav-btn-list {
        margin-top: 32px;     
        border-top: 1px solid #ccc;   
      }
      ::slotted(menu-button-comp) {
        border-bottom: 1px solid #ccc;        
      }

      .overlay {
        background-color: rgba(0,0,0,0.5);
        cursor: pointer;
        z-index: 2;
      }
    `;
  }

  public openSideNav() {
    const overlay = this.getElementById("nav-overlay");
    if (overlay) { overlay.style.display = 'block'; }
    const sidenav = this.getElementById("side-nav");
    if (sidenav) { sidenav.style.display = 'block'; }
  }

  public closeSideNav() {
    const overlay = this.getElementById("nav-overlay");
    if (overlay) { overlay.style.display = 'none'; }
    const sidenav = this.getElementById("side-nav");
    if (sidenav) { sidenav.style.display = 'none'; }
  }
}