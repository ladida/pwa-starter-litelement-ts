import { html, css, customElement, property } from 'lit-element';
import { BaseComp } from '../../../core/gui/BaseComp';
import '../button/common/blue-button-comp.ts';
import '../button/common/red-button-comp.ts';
import '../button/common/green-button-comp.ts';

@customElement('confirm-dialog')
export class ConfirmDialog extends BaseComp {

  @property()
  public headline: string;
  @property()
  public text: string;

  @property()
  private isOpen: boolean = false;

  private clickYesEvent: Event;

  constructor() {
    super();
    this.clickYesEvent = new Event('clickYes');
  }

  render() {
    return html`${this.isOpen ? html`
      <div class="overlay">
        <div class="dialog">
          <div class="content">
            <div class="headline">${this.headline}</div>
            <div class="text">${this.text}</div>
          </div>
          <div class="footer">
            <div class="buttons">
              <red-button-comp title="Yes" style="width: 100px; margin-right: 20px;" @click="${this.clickYes}"></red-button-comp>
              <blue-button-comp title="No" style="width: 100px;" @click="${this.clickNo}"></blue-button-comp>
            </div>
          </div>
        </div>
      </div>
    `: ''}`;
  }

  public open() {
    this.isOpen = true;
  }

  public close() {
    this.isOpen = false;
  }

  private clickYes() {
    this.dispatchEvent(this.clickYesEvent);
  }

  private clickNo() {
    this.close();
  }

  static get styles() {
    return css`
      .overlay {
        position: absolute;
        width: 100vw;
        height: 100vh;
        z-index: 90;
        background-color: rgba(0, 0, 0, 0.4);
        top: 0px;
        left: 0px;
      }
      .dialog {
        position: absolute;
        width: 400px;
        min-width: 300px;
        top: calc(50% - 100px);
        left: calc(50% - 200px);
        z-index: 95;
        background-color: white;
        border-radius: 8px;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)
      }
      .content {
        padding: 24px;
      }
      .headline {
        font-size: 20px;
        font-weight: bold;
      }
      .text {
        margin-top: 20px;
      }
      .footer {
        padding: 12px;
        background-color: lightgrey;
        border-radius: 0px 0px 8px 8px;
      }
      .buttons {
        width: max-content;
        display: flex;
        margin-left: auto;
      }

      @media only screen and (max-width: 400px) {
        .dialog {
          width: 100%;
          border-radius: 0px;
          left: 0px;
        }
      }
    `;
  }
}