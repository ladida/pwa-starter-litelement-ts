import { html, css, customElement, property } from 'lit-element';
import { BaseComp } from '../../../core/gui/BaseComp';

@customElement('textfield-comp')
export class TextfieldComp extends BaseComp {
  
  @property()
  public value: string = '';

  @property()
  public placeholder: string = '';

  render() {
    return html`
      <input class="input" type="text" placeholder="${this.placeholder}" .value="${this.value}" @change="${this.valueChange}"/>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .input {
        padding: 8px;
        border: none;
        border-bottom: 1px solid #ccc;
        width: 100%;
        box-sizing: border-box;
        font-size: 15px;
      }
      .input:focus {
        outline: unset;
        border-bottom: 1px solid #000000;
      }
    `;
  }

  private valueChange(e: { target: HTMLInputElement }) {
    this.value = e.target.value;
  }
}