import { openDB, IDBPDatabase } from 'idb';
import { Schema, SchemaNames } from '../../../../model/schema/Schema';

/**
 * Handles the connection to the indexeddb
 */
export class IndexedDbConnection {

  private db: IDBPDatabase<Schema>;


  public async getOpenedDb(): Promise<IDBPDatabase<Schema>> {
    if (!this.db) {
      await this.openDb();
    }
    return this.db;
  }

  /**
    * Opens the db and create/update schemas.
    */
  private async openDb() {
    if (!window.indexedDB) {
      window.alert("Ihr Browser unterstützt keine stabile Version von IndexedDB. Dieses und jenes Feature wird Ihnen nicht zur Verfügung stehen.");
    }

    this.db = await openDB<Schema>(SchemaNames.DB_NAME, 1, {
    });
  }
}

/**
 * Factory for the IndexedDbConnection.
 */
export class IndexedDbConnectionFactory {
  private static instance: IndexedDbConnection;

  public static async getInstance(): Promise<IndexedDbConnection> {
    if (!IndexedDbConnectionFactory.instance) {
      IndexedDbConnectionFactory.instance = new IndexedDbConnection();
    }
    return IndexedDbConnectionFactory.instance;
  }
}