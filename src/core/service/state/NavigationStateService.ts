import { StateService } from "./StateService";
import { MainViews } from "../../../enum/MainViews";

/**
 * State service for navigation.
 */
export class NavigationStateService extends StateService {

  /**
   * the view
   * start at the todolist view
   */
  private actualView: MainViews = MainViews.DEMO;

  public async init() {
  }

  public getActualView() {
    return this.actualView;
  }
  
  public setDemoView() {
    this.actualView = MainViews.DEMO;
    this.informObservers();
  }

  public setAboutView() {
    this.actualView = MainViews.ABOUT;
    this.informObservers();
  }
}

/**
 * Factory for the NavigationStateService.
 */
export class NavigationStateServiceFactory {
  private static instance: NavigationStateService;

  public static async getInstance(): Promise<NavigationStateService> {
    if (!NavigationStateServiceFactory.instance) {
      NavigationStateServiceFactory.instance = new NavigationStateService();
      await NavigationStateServiceFactory.instance.init();
    }
    return NavigationStateServiceFactory.instance;
  }
}