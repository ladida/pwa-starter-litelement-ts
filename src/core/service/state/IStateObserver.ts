/**
 * Interface for a state observer
 */
export interface IStateObserver {
  stateChanged(): void;
}