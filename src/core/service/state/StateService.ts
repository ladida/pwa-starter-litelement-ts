import { BaseService } from "../BaseService";
import { IStateObserver } from "./IStateObserver";

/**
 * Base class for all state services
 * 
 * uses the observer pattern
 */
export abstract class StateService extends BaseService {

  private observers: IStateObserver[] = [];

  /**
   * add observer
   * 
   * @param observer new observer
   */
  public registerObserver(observer: IStateObserver) {
    this.observers.push(observer);
  }

  /**
   * remove observer
   * 
   * @param observer observer to remove
   */
  public unregisterObserver(observer: IStateObserver) {
    this.observers = this.observers.filter(obs => obs != observer);
  }

  /**
   * inform all observers
   */
  public informObservers() {
    for (let observer of this.observers) {
      observer.stateChanged();
    }
  }
}