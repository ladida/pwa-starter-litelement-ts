export abstract class BaseService {

  public abstract async init(): Promise<void>;
}