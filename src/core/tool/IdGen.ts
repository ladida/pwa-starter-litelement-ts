export class IdGen {
  private static CHARS: string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  public static generate(count: number) {
    let id = "";
    for (let i = 0; i < count; ++i) {
      id += this.getRandomChar();
    }

    return id;
  }

  private static getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  private static getRandomChar(): string {
    return IdGen.CHARS.charAt(Math.floor(Math.random() * Math.floor(IdGen.CHARS.length)));
  }
}