import { LitElement } from 'lit-element';


/**
 * Base component.
 */
export abstract class BaseComp extends LitElement {

  /**
   * Get the element by id.
   * 
   * @param id id
   */
  protected getElementById(id: string) {
    return this.shadowRoot?.getElementById(id);
  }
}
