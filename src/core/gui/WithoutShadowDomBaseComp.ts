import { BaseComp } from "./BaseComp";

/**
 * Base component without shadow dom.
 */
export abstract class WithoutShadowDomBaseComp extends BaseComp {
  
  /**
   * Get the element by id.
   * 
   * @param id id
   */
  protected getElementById(id: string) {
    return document.getElementById(id);
  }
  
  /**
   * no shadow dom
   */
  protected createRenderRoot() {
    return this;
  }
}
