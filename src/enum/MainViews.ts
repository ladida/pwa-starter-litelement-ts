/**
 * Represent the navigation targets for the main view
 */
export class MainViews {
  public static DEMO: string = 'DEMO';
  public static ABOUT: string = 'ABOUT';
}
