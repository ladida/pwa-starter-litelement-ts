if ('workbox' in self) {
  workbox.precaching.precacheAndRoute(self.__precacheManifest || []);
  console.log('[service-worker.js] Caching erfolgreich');
}

self.addEventListener("message", (evt) => {
  if ("SKIP_WAITING" == evt.data) {
    self.skipWaiting();
  } else if ("CLAIM" == evt.data) {
    self.clients.claim();
  }
});