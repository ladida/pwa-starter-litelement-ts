# todo-pwa-litelement-ts

PWA starter for Lit-Element with TypeScript.

# Accessible under

https://ladida.gitlab.io/pwa-starter-litelement-ts/

# Installation

```sh
npm i
```

# Run

dev:
```sh
npm run dev
```

dev with service worker enabled:
```sh
npm run dev:sw
```

Prod:
```sh
npm run prod
cd dist
serve .
```
